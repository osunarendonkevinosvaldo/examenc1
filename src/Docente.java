/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hp
 */
public class Docente extends Empleado {
    private int nivel;
    private float horasLab;
    private float pagoPorHora;

    public Docente() {
        this.nivel = 0;
        this.horasLab = 0.0f;
        this.pagoPorHora = 0.0f;
    }

    public Docente(int nivel, float horasLab, float pagoPorHora, int numEmpleado, String nombre, String domicilio, Contrato contrato) {
        super(numEmpleado, nombre, domicilio, contrato);
        this.nivel = nivel;
        this.horasLab = horasLab;
        this.pagoPorHora = pagoPorHora;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHorasLab() {
        return horasLab;
    }

    public void setHorasLab(float horasLab) {
        this.horasLab = horasLab;
    }

    public float getPagoPorHora() {
        return pagoPorHora;
    }

    public void setPagoPorHora(float pagoPorHora) {
        this.pagoPorHora = pagoPorHora;
    }
    
    
    @Override
    public float calcularTotal() {
        float total= 0.0f;
        if (this.nivel == 1){
            total = (this.horasLab * this.pagoPorHora)*0.35f;
        }
        if (this.nivel == 2){
            total = (this.horasLab * this.pagoPorHora)*0.40f;
        }
        if (this.nivel == 3){
            total = (this.horasLab * this.pagoPorHora)*0.50f;
        }
        return total;
    }

    @Override
    public float calcularImpuesto() {
       return (this.horasLab * this.pagoPorHora)*(contrato.getImpuestoISR()/100);
      
    }
    
    
    
}
