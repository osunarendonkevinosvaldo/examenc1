/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hp
 */
public class Contrato {
    private String claveCont;
    private String puesto;
    private float impuestoISR;

    public Contrato() {
        this.claveCont = "";
        this.puesto = "";
        this.impuestoISR = 0.0f;
    }

    public Contrato(String claveCont, String puesto, float impuestoISR) {
        this.claveCont = claveCont;
        this.puesto = puesto;
        this.impuestoISR = impuestoISR;
    }

    public String getClaveCont() {
        return claveCont;
    }

    public void setClaveCont(String claveCont) {
        this.claveCont = claveCont;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoISR() {
        return impuestoISR;
    }

    public void setImpuestoISR(float impuestoISR) {
        this.impuestoISR = impuestoISR;
    }
    
    
    
}
